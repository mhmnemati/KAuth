const { Pool } = require('pg');

// object: global object ( global variables and database connection and etc )
module.exports = {
    connection: undefined,

    // method: dbms initializer
    set: async (host, port, user, password, db) => {
        this.connection = new Pool({
            host: host,
            port: port,
            user: user,
            password: password,
            database: db
        });
        await this.connection.query('CREATE TABLE IF NOT EXISTS kauth_users(id TEXT PRIMARY KEY, groups JSONB, detail JSONB)');
        await this.connection.query('CREATE TABLE IF NOT EXISTS kauth_groups(name TEXT PRIMARY KEY, parents TEXT, owners TEXT, permissions JSONB)');
        await this.connection.query('INSERT INTO kauth_users(id, groups, detail) VALUES($1,$2,$3)', ['root', { root: true }, {}]).catch(exception => false);
        await this.connection.query('INSERT INTO kauth_groups(name, parents, owners, permissions) VALUES($1,$2,$3,$4)', ['root', 'root', 'root', {}]).catch(exception => false);
        return true;
    },

    get: () => {
        return this.connection;
    }

};