test('Connection Test', async () => {
    expect.assertions(1);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    expect(
        await kauth.connect()
    ).toBe(true);
    await kauth.User.Delete('test');
});