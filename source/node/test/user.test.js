test('Create, Delete User Test', async () => {
    expect.assertions(2);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    expect(
        await kauth.User.Create('test', ['gp1', 'gp2'], { password: '123' })
    ).toBe(true);
    expect(
        await kauth.User.Delete('test')
    ).toBe(true);
});

test('Add, Remove, Set, Clear Detail User Test', async () => {
    expect.assertions(4);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    await kauth.User.Create('test', [], {}).catch(exception => false);
    expect(
        await kauth.User.AddDetail('test', 'key', 'value')
    ).toBe(true);
    expect(
        await kauth.User.RemoveDetail('test', 'key')
    ).toBe(true);
    expect(
        await kauth.User.SetDetail('test', { key: 'value' })
    ).toBe(true);
    expect(
        await kauth.User.ClearDetail('test')
    ).toBe(true);
    await kauth.User.Delete('test').catch(exception => false);
});

test('Get Detail, Details, Groups, Permission, Permissions User Test', async () => {
    expect.assertions(5);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    await kauth.User.Create('test', [], {}).catch(exception => false);
    await kauth.User.SetDetail('test', { key: 'value' }).catch(exception => false);
    expect(
        await kauth.User.GetDetail('test', 'key')
    ).toBe('value');
    expect(
        Object.keys(await kauth.User.GetDetails('test')).length
    ).toBe(1);
    expect(
        Object.keys(await kauth.User.GetGroups('test')).length
    ).toBe(0);
    expect(
        await kauth.User.GetPermission('test', 'permission')
    ).toBe(false);
    expect(
        Object.keys(await kauth.User.GetPermissions('test')).length
    ).toBe(0);
    await kauth.User.Delete('test').catch(exception => false);
});
