test('Create, Delete Group Test', async () => {
    expect.assertions(2);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    expect(
        await kauth.Group.Create('root', 'test', 'root')
    ).toBe(true);
    expect(
        await kauth.Group.Delete('root', 'test')
    ).toBe(true);
});

test('Add, Remove, Set, Clear Permission Group Test', async () => {
    expect.assertions(4);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    await kauth.Group.Create('root', 'test', 'root').catch(exception => false);
    expect(
        await kauth.Group.AddPermission('root', 'test', 'permission')
    ).toBe(true);
    expect(
        await kauth.Group.RemovePermission('root', 'test', 'permission')
    ).toBe(true);
    expect(
        await kauth.Group.SetPermission('root', 'test', ['permission1', 'permission2']),
    ).toBe(true);
    expect(
        await kauth.Group.ClearPermission('root', 'test'),
    ).toBe(true);
    await kauth.Group.Delete('root', 'test').catch(exception => false);
});

test('Add, Remove User Group Test', async () => {
    expect.assertions(2);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    await kauth.User.Create('test', [], {}).catch(exception => false);
    expect(
        await kauth.Group.AddUser('root', 'root', 'test')
    ).toBe(true);
    expect(
        await kauth.Group.RemoveUser('root', 'root', 'test')
    ).toBe(true);
    await kauth.User.Delete('test').catch(exception => false);
});

test('Get Permissions, Users Group Test', async () => {
    expect.assertions(2);
    const { KAuth } = require('../src/app');
    var kauth = new KAuth('172.17.0.2', 5432, 'koliber', '140277mhn', 'tgs');
    await kauth.connect().catch(exception => false);
    await kauth.User.Create('test', [], {}).catch(exception => false);
    await kauth.Group.AddPermission('root', 'root', 'permission').catch(exception => false);
    await kauth.Group.AddUser('root', 'root', 'test').catch(exception => false);
    expect(
        Object.keys(await kauth.Group.GetPermissions('root')).length
    ).toBe(1);
    expect(
        Object.keys(await kauth.Group.GetUsers('root')).length
    ).toBe(2);
    await kauth.Group.RemovePermission('root', 'root', 'permission').catch(exception => false);
    await kauth.Group.RemoveUser('root', 'root', 'test').catch(exception => false);
    await kauth.User.Delete('test').catch(exception => false);
});