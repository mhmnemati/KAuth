# Application Interface

## Library API

### Group

* __Function__: `kauth.Group.{Command}(params)`
* __Info__: Async function
* __Commands__:

    | Command              | Description                  |
    | -------------------- | ---------------------------- |
    | __Create__           | Create a group               |
    | __Delete__           | Delete a group               |
    | __AddPermission__    | Add permission to group      |
    | __RemovePermission__ | Remove permission from group |
    | __SetPermission__    | Set permissions to group     |
    | __ClearPermission__  | Clear permissions from group |
    | __AddUser__          | Add user to group            |
    | __RemoveUser__       | Remove user from group       |
    | __GetPermissions__   | Get all permissions of group |
    | __GetUsers__         | Get all users of group       |

#### Create

* __params__: `(id, name, parent)`

    | Parameter     | Value Type    | Description                 |
    | ------------- | ------------- | --------------------------- |
    | __id__        | __string__    | Creator user's ID           |
    | __name__      | __string__    | Group's name                |
    | __parent__    | __string__    | Parent group's name         |

* __result__: boolean

#### Delete

* __params__: `(id, name)`

    | Parameter     | Value Type    | Description           |
    | ------------- | ------------- | --------------------- |
    | __id__        | __string__    | Creator user's ID     |
    | __name__      | __string__    | Group's name          |

* __result__: boolean

#### AddPermission

* __params__: `(id, name, permission)`

    | Parameter       | Value Type    | Description             |
    | --------------- | ------------- | ----------------------- |
    | __id__          | __string__    | Creator user's ID       |
    | __name__        | __string__    | Group's name            |
    | __permission__  | __string__    | Group's permission      |

* __result__: boolean

#### RemovePermission

* __params__: `(id, name, permission)`

    | Parameter      | Value Type    | Description        |
    | -------------- | ------------- | ------------------ |
    | __id__         | __string__    | Creator user's ID  |
    | __name__       | __string__    | Group's name       |
    | __permission__ | __string__    | Group's permission |

* __result__: boolean

#### SetPermission

* __params__: `(id, name, permission)`

    | Parameter      | Value Type    | Description        |
    | -------------- | ------------- | ------------------ |
    | __id__         | __string__    | Creator user's ID  |
    | __name__       | __string__    | Group's name       |
    | __permission__ | __array__     | Group's permission |

* __result__: boolean

#### ClearPermission

* __params__: `(id, name)`

    | Parameter     | Value Type    | Description       |
    | ------------- | ------------- | ----------------- |
    | __id__        | __string__    | Creator user's ID |
    | __name__      | __string__    | Group's name      |

* __result__: boolean

#### AddUser

* __params__: `(id, name, user_id)`

    | Parameter     | Value Type    | Description       |
    | ------------- | ------------- | ----------------- |
    | __id__        | __string__    | Creator user's ID |
    | __name__      | __string__    | Group's name      |
    | __user\_id__  | __string__    | Addable user's ID |

* __result__: boolean

#### RemoveUser

* __params__: `(id, name, user_id)`

    | Parameter     | Value Type    | Description         |
    | ------------- | ------------- | ------------------- |
    | __id__        | __string__    | Creator user's ID   |
    | __name__      | __string__    | Group's name        |
    | __user\_id__  | __string__    | Removable user's ID |

* __result__: boolean

#### GetPermissions

* __params__: `(name)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __name__      | __string__    | Group's name  |

* __result__: object

#### GetUsers

* __params__: `(name)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __name__      | __string__    | Group's name  |

* __result__: object

_______________

### User

* __Function__: `kauth.User.{Command}(params)`
* __Info__: Async function
* __Commands__:

    | Command            | Description                 |
    | ------------------ | --------------------------- |
    | __Create__         | Create a user               |
    | __Delete__         | Delete a user               |
    | __AddDetail__      | Add detail to user          |
    | __RemoveDetail__   | Remove detail from user     |
    | __SetDetail__      | Set detail to user          |
    | __ClearDetail__    | Clear detail from user      |
    | __GetDetail__      | Get detail of user          |
    | __GetDetails__     | Get all details of user     |
    | __GetGroups__      | Get all groups of user      |
    | __GetPermission__  | Get permission of user      |
    | __GetPermissions__ | Get all permissions of user |

#### Create

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: boolean

#### Delete

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: boolean

#### AddDetail

* __params__: `(id, key, value)`

    | Parameter     | Value Type    | Description    |
    | ------------- | ------------- | -------------- |
    | __id__        | __string__    | User's ID      |
    | __key__       | __string__    | Detail's key   |
    | __value__     | __string__    | Detail's value |

* __result__: boolean

#### RemoveDetail

* __params__: `(id, key)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |
    | __key__       | __string__    | Detail's key  |

* __result__: boolean

#### SetDetail

* __params__: `(id, detail)`

    | Parameter     | Value Type    | Description     |
    | ------------- | ------------- | --------------- |
    | __id__        | __string__    | User's ID       |
    | __detail__    | __object__    | Detail's object |

* __result__: boolean

#### ClearDetail

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: boolean

#### GetDetail

* __params__: `(id, key)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |
    | __key__       | __string__    | Detail's key  |

* __result__: string

#### GetDetails

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: object

#### GetGroups

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: objectobject

#### GetPermission

* __params__: `(id, permission)`

    | Parameter      | Value Type    | Description       |
    | -------------- | ------------- | ----------------- |
    | __id__         | __string__    | User's ID         |
    | __permission__ | __string__    | User's permission |

* __result__: boolean

#### GetPermissions

* __params__: `(id)`

    | Parameter     | Value Type    | Description   |
    | ------------- | ------------- | ------------- |
    | __id__        | __string__    | User's ID     |

* __result__: object

_______________